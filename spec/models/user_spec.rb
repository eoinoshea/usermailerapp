# == Schema Information
#
# Table name: users
#
#  id                     :integer          not null, primary key
#  name                   :string           default(""), not null
#  email                  :string           default(""), not null
#  encrypted_password     :string           default(""), not null
#  recipient_id           :integer
#  reset_password_token   :string
#  reset_password_sent_at :datetime
#  remember_created_at    :datetime
#  sign_in_count          :integer          default("0"), not null
#  current_sign_in_at     :datetime
#  last_sign_in_at        :datetime
#  current_sign_in_ip     :string
#  last_sign_in_ip        :string
#  emails_users_id        :integer
#  created_at             :datetime         not null
#  updated_at             :datetime         not null
#

require 'rails_helper'

RSpec.describe User, type: :model do



  before do
    @user = FactoryGirl.create(:user)
  end

  subject { @user }

  it { should validate_uniqueness_of(:name) }

  describe '#send_mail' do

    it 'should sucessfully deliver a mail with the right parameters' do

    end

    it 'should fail to deliver a mail with an invalid recipient' do

    end

  end
end
