# == Schema Information
#
# Table name: emails
#
#  id              :integer          not null, primary key
#  subject_text    :string
#  body_text       :string
#  recipient_email :string
#  emails_users_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

FactoryGirl.define do
  factory :email do
    subject_text "test email subject"
    body_text "Hello this is a test email content"
  end
end
