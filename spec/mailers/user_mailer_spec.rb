require "rails_helper"
require 'mailgun_rails/deliverer'
require 'mailgun_rails/client'
require 'json'

RSpec.describe UserMailer, type: :mailer do

  describe "#send_email"

  before do :all
    @sender = FactoryGirl.create(:user, email: 'eoinosh.developer@gmail.com')
    @recipient = FactoryGirl.create(:user, email: 'spungex@hotmail.com')
    @email = FactoryGirl.create(:email)
  end


  it 'should send an email with mailgun' do
      expect{UserMailer.send_mail(@sender,@email,@recipient).deliver_now}
    end

end
