UserMailerApp Details

* Ruby version
  - 2.2.1

* Raiils version
  - 4.2.8

* System dependencies
  - postgresql

* Configuration
  - rvm use 2.2.1 (or rbenv)
  - gem install rails -v 4.1.8
  - bundle install
  - install dependencies
  - CREATE USER users_email_app_admin  WITH PASSWORD '4691users';
  CREATE USER users_email_app_admin  WITH PASSWORD '4691users';
  ALTER USER users_email_app_admin  CREATEDB;

* Database creation
  - rake db:create && rake db:migrate && rake db:seed && rake db:test:prepare

* Seed Database with users
  - rake db:seed

* How to run the test suite
  - smoke test
'run rspec/controllers/*'


* Full test suite mailers/models/controllers
  - run'rspec' covers models, mailers, controller. requests