class CreateEmails < ActiveRecord::Migration
  def change
    create_table :emails do |t|
      t.string :subject_text
      t.string :body_text
      t.string :recipient_email
      t.references :emails_users

      t.timestamps null: false
    end
  end
end
