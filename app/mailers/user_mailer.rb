class UserMailer < ApplicationMailer

  def send_mail(user,mail,recipient)
    @user = user
    @mail = mail
    @recipient = recipient
    mail (from: user.email,
         to: recipient.email,
         subject: mail.subject_text,
          text: "",
          content_type: 'text/html',
          template_path: 'mailers',
          template_name: '/user_mailer')
  end

end
