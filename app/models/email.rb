# == Schema Information
#
# Table name: emails
#
#  id              :integer          not null, primary key
#  subject_text    :string
#  body_text       :string
#  recipient_email :string
#  emails_users_id :integer
#  created_at      :datetime         not null
#  updated_at      :datetime         not null
#

class Email < ActiveRecord::Base
  #vaidates :recipient_email, presence: true

  has_many :emails_users
  has_many :users, :through => :emails_users

end
