# == Schema Information
#
# Table name: emails_users
#
#  email_id :integer          not null
#  user_id  :integer          not null
#

class EmailsUser < ActiveRecord::Base

  belongs_to :user
  belongs_to :email

end
